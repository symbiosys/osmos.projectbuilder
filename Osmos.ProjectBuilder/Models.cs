﻿using Osmos.CmdExecuter;

namespace Osmos.ProjectBuilder
{
    public enum BuildConfiguration
    {
        Debug,
        Release
    }

    public enum BuildStatus
    {
        None,
        Error,
        Warning,
        Success
    }

    public enum BuildTarget
    {
        Project,
        Solution
    }

    public class BuilderOptions
    {
        public BuilderOptions()
        {

        }

        public BuilderOptions(BuilderOptions options)
        {
            this.FilePath = options.FilePath;
            this.MsBuildPath = options.MsBuildPath;
            this.LoggingService = options.LoggingService;
        }

        public string FilePath { get; set; }
        public string MsBuildPath { get; set; }
        public ILoggingService LoggingService { get; set; }
    }

    public class BadBuildTargetException : System.Exception
    {
        public BadBuildTargetException(string fileExtension)
            : base(string.Format("Unexpected file extension {0}", fileExtension)) { }
    }
}
