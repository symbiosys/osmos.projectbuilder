﻿using Osmos.CmdExecuter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Osmos.ProjectBuilder
{
    public class Builder
    {
        public static Builder Create(BuilderOptions options)
        {
            var lOptions = _ValidateOptions(options);
            var builder = new Builder();

            builder._msBuildPath = lOptions.MsBuildPath;
            builder._filePath = lOptions.FilePath;
            builder._fileName = Path.GetFileName(lOptions.FilePath);
            builder._buildStatus = BuildStatus.None;
            builder._buildTarget = _BuildTarget(Path.GetExtension(lOptions.FilePath));
            builder._loggingService = lOptions.LoggingService;

            return builder;
        }
        public async Task BuildAsync(BuildConfiguration buildConfiguration = BuildConfiguration.Release)
        {
            _LogStart(buildConfiguration);

            string arguments = string.Format("{0} /p:Configuration=\"{1}\"", _fileName, buildConfiguration);
            var options = new ExecuterOptions
            {
                FilePath = _msBuildPath,
                WorkingDirectory = Path.GetDirectoryName(_filePath),
                Arguments = arguments,
                LoggingService = _loggingService
            };
            var cmdExecuter = Executer.Create(options);

            await cmdExecuter.TryExecuteAsync();

            _warningsCount = _GetWarningsCount(cmdExecuter.Outputs);
            _errorsCount = _GetErrorsCount(cmdExecuter.Outputs);

            if (_errorsCount != 0)
            {
                _buildStatus = BuildStatus.Error;
                goto End;
            }

            if (_warningsCount != 0)
            {
                _buildStatus = BuildStatus.Warning;
                goto End;
            }

            if (!cmdExecuter.IsSuccess)
            {
                _buildStatus = BuildStatus.Error;
                goto End;
            }

            _buildStatus = BuildStatus.Success;

        End:
            _LogEnd();
        }

        public int ErrorsCount
        {
            get
            {
                return _errorsCount;
            }
        }
        public int WarningsCount
        {
            get
            {
                return _warningsCount;
            }
        }
        public BuildStatus BuildStatus
        {
            get {
                return _buildStatus;
            }
        }
        public BuildTarget BuildTarget
        {
            get
            {
                return _buildTarget;
            }
        }

        #region Private
        string _msBuildPath;
        string _filePath;
        string _fileName;
        ILoggingService _loggingService;
        BuildStatus _buildStatus;
        BuildTarget _buildTarget;
        int _errorsCount = 0, _warningsCount = 0;

        private static BuilderOptions _ValidateOptions(BuilderOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("BuilderOptions object can not be null.");

            var lOptions = new BuilderOptions(options);
            if (lOptions.FilePath == null)
                throw new ArgumentNullException("FilePath can not be null.");
            if (lOptions.MsBuildPath == null)
                lOptions.MsBuildPath = @"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe";
            if (!File.Exists(lOptions.MsBuildPath))
                throw new FileNotFoundException(string.Format("Can not find MSBuild in \"{0}\"", lOptions.MsBuildPath));
            if (!File.Exists(lOptions.FilePath))
                throw new FileNotFoundException(string.Format("Can not find the file in \"{0}\"", lOptions.FilePath));

            return lOptions;
        }

        private static BuildTarget _BuildTarget(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".sln":
                    return BuildTarget.Solution;
                case ".csproj":
                    return BuildTarget.Project;
                default:
                    throw new BadBuildTargetException(fileExtension);
            }
        }

        private void _LogStart(BuildConfiguration buildConfiguration)
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Started Building \"{0}\"", _fileName));
            _loggingService.Log(string.Format("Build config \"{0}\"", buildConfiguration));
        }

        private void _LogEnd()
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Finished Building \"{0}\"", _fileName));
            _loggingService.Log(string.Format("Warnings Count : {0}", _warningsCount));
            _loggingService.Log(string.Format("Errors Count : {0}", _errorsCount));
            _loggingService.Log(string.Format("Build Status : \"{0}\"", _buildStatus));
        }

        private int _GetWarningsCount(List<string> outputs) {
            foreach (var output in outputs)
            {
                int index = output.LastIndexOf(" Warning(s)");
                if (index > -1)
                {
                    string str = output.Substring(0, index + 1);
                    return Int32.Parse(str);
                }
            }
            return 0;
        }

        private int _GetErrorsCount(List<string> outputs)
        {
            foreach (var output in outputs)
            {
                int index = output.LastIndexOf(" Error(s)");
                if (index > -1)
                {
                    string str = output.Substring(0, index + 1);
                    return Int32.Parse(str);
                }
            }
            return 0;
        }
        #endregion

    }
}
