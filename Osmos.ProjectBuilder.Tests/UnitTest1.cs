﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Osmos.CmdExecuter;
using System.IO;
using System.Threading.Tasks;

namespace Osmos.ProjectBuilder.Tests
{
    class LoggingService : ILoggingService
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }
    }

    [TestClass]
    public class UnitTest1
    {
        string _rootPath;

        public UnitTest1()
        {
            _rootPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName).FullName;
        }

        [TestMethod]
        public void empty()
        {
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void create_validation_method_exception_expected_for_null_options() {
            var builder = Builder.Create(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void create_validation_method_exception_expected_for_null_filepath()
        {
            var builder = Builder.Create(new BuilderOptions { });
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void create_validation_method_exception_expected_for_bad_filepath()
        {
            var builder = Builder.Create(new BuilderOptions {
                FilePath = "wrong_file_path.error"
            });
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void create_validation_method_exception_expected_for_bad_msbuildpath()
        {
            string filePath = Path.Combine(_rootPath, @"TestSolutions\TestProject.Working\TestProject.Working.sln");
            var builder = Builder.Create(new BuilderOptions
            {
                FilePath = filePath,
                MsBuildPath = "wrong_msbuild_path.error"
            });
        }

        [TestMethod]
        public async Task working_solution_build()
        {
            string filePath = Path.Combine(_rootPath, @"TestSolutions\TestProject.Working\TestProject.Working.sln");
            var builder = Builder.Create(new BuilderOptions
            {
                FilePath = filePath,
                LoggingService = new LoggingService()
            });

            await builder.BuildAsync();

            Assert.AreEqual(builder.ErrorsCount, 0);
            Assert.AreEqual(builder.WarningsCount, 0);
            Assert.AreEqual(builder.BuildStatus, BuildStatus.Success);
        }

        [TestMethod]
        public async Task warning_solution_build()
        {
            string filePath = Path.Combine(_rootPath, @"TestSolutions\TestProject.Warning\TestProject.Warning.sln");
            var builder = Builder.Create(new BuilderOptions
            {
                FilePath = filePath,
                LoggingService = new LoggingService()
            });

            await builder.BuildAsync();

            Assert.AreEqual(builder.ErrorsCount, 0);
            Assert.IsTrue(builder.WarningsCount >= 0);
        }

        [TestMethod]
        public async Task error_solution_build()
        {
            string filePath = Path.Combine(_rootPath, @"TestSolutions\TestProject.Error\TestProject.Error.sln");
            var builder = Builder.Create(new BuilderOptions
            {
                FilePath = filePath,
                LoggingService = new LoggingService()
            });

            await builder.BuildAsync();

            Assert.AreNotEqual(builder.ErrorsCount, 0);
            Assert.AreEqual(builder.WarningsCount, 0);
            Assert.AreEqual(builder.BuildStatus, BuildStatus.Error);
        }
    }
}
