﻿using Osmos.CmdExecuter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osmos.ProjectBuilder.ConsoleApp
{
    class LoggingService : ILoggingService
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string rootPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName).FullName;
            string filePath = Path.Combine(rootPath, @"TestSolutions\TestProject.Working\TestProject.Working.sln");

            var builder = Builder.Create(new BuilderOptions
            {
                FilePath = filePath,
                LoggingService = new LoggingService()
            });

            var task = Task.Run(async () => await builder.BuildAsync(BuildConfiguration.Debug));
            task.Wait();
        }
    }
}
